<?php
/*
 * Seatin Charts for events and users
 */

add_action( 'admin_print_scripts-post-new.php', 'cyc_seating_chart_scripts', 11 );
add_action( 'admin_print_scripts-post.php', 'cyc_seating_chart_scripts', 11 );

function cyc_seating_chart_scripts() {
  global $post_type;
  if( 'seating-chart' == $post_type ){
	wp_enqueue_script( 'seating-chart-admin-js', get_stylesheet_directory_uri() . '/assets/js/seating-chart-admin.js' );
	wp_enqueue_style( 'seating-chart-admin-css', get_stylesheet_directory_uri() . '/assets/css/seating-chart-admin.css' );
  }
}

add_action( 'cmb2_init', 'cyc_seating_chart_metabox' );

function cyc_seating_chart_create(){
	return '<div id="seating-chart-ui"><div class="seating-chart-ui-header"></div><div class="seating-chart-ui-body"><ul  id="seating-chart-spots"></ul><button id="btnClear" class="button button-large" type="button">Clear Changes</button> <button id="btnShow" class="button button-primary button-large" type="button">Set Seating Chart</button></div></div>';
}

function cyc_seating_chart_metabox() {

  $cmb = new_cmb2_box( array(
	'id'           => 'chartcreator',
	'title'        => __( 'Seating Cart', 'cmb2' ),
	'object_types' => array( 'seating-chart' ),
	'context'      => 'normal',
	'priority'     => 'default',
  ) );

  $cmb->add_field( array(
	'name' => __( 'Chart Spots', 'cmb2' ),
	'id' => 'chart_spots',
	'before_row'   => cyc_seating_chart_create(),
	'type' => 'text',
  ) );

}
