<?php
/*
 * Class Schedule (ClassCal)
 */

// TO DO:
// Add ACF into plugin
// Rename Instructor & Studio slugs and rename in metaboxes/admin_enqueue_scripts
// Combine and minify css/js
// Change seating grid css (colors) to be more generic on backend
// Add plugin options page
// Change urls for scripts/css and instructor image
// Add transients to ajax get studio active seats (delete transient on update/save studio)
// Add rows and colums to seating chart admin

// Custom Post Types
add_action( 'init', 'classcal_cpt' );
function classcal_cpt() {
	$labels = array(
		"name" => __( 'Classes', 'cycmode' ),
		"singular_name" => __( 'Class', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Classes', 'cycmode' ),
		"labels" => $labels,
		"menu_icon" => "dashicons-welcome-learn-more",
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "class", "with_front" => true ),
		"query_var" => true,

		//"supports" => array( "title" ),
	);
	register_post_type( "classcal_classes", $args );

	$labels = array(
		"name" => __( 'Class Schedules', 'cycmode' ),
		"singular_name" => __( 'Class Schedule', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Class Schedule', 'cycmode' ),
		"labels" => $labels,
		"menu_icon" => "dashicons-welcome-learn-more",
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "class-schedule", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "class_schedules", $args );

	$labels = array(
		"name" => __( 'Class Reservations', 'cycmode' ),
		"singular_name" => __( 'Class Reservation', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Class Reservation', 'cycmode' ),
		"labels" => $labels,
		"menu_icon" => "dashicons-welcome-learn-more",
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "class-reservation", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "class_reservations", $args );

	$labels = array(
		"name" => __( 'Studios', 'cycmode' ),
		"singular_name" => __( 'Studio', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Studios', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-location-alt",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "studio", "with_front" => true ),
		"query_var" => true,
		'register_meta_box_cb' => 'classcal_studio_seating_metabox',

		"supports" => array( "title" ),
	);
	register_post_type( "studios", $args );

	$labels = array(
		"name" => __( 'Instructors', 'cycmode' ),
		"singular_name" => __( 'Instructor', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Instructors', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-admin-users",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "instructor", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "instructors", $args );
}

// Taxonomies - for class types
add_action( 'init', 'register_classcal_cat' );
function register_classcal_cat() {
	$labels = array(
		"name" => __( 'Class Types', 'cycmode' ),
		"singular_name" => __( 'Class Type', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Class Types', 'cycmode' ),
		"labels" => $labels,
		"public" => true,
		"hierarchical" => true,
		"label" => "Class Types",
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'class-category', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"show_in_quick_edit" => false,
		"meta_box_cb" => false
	);
	register_taxonomy( "classcal_cat", array( "classcal_classes" ), $args );
}

// Colors


// Frontend Scripts + Styles
function classcal_client_scripts() {
	if ( is_page_template('page-classcal.php') ) {
		wp_enqueue_style( 'classcal-client-css', get_template_directory_uri() . '/assets/css/classcal/classcal-client.css');
		wp_enqueue_script( 'modernizer', get_template_directory_uri() . '/assets/js/modernizer.js', array('jquery'), 'null', true );
		wp_enqueue_script( 'css-modal', get_template_directory_uri() . '/assets/js/modal.js', array('jquery'), 'null', true );
		wp_enqueue_script( 'classcal-client-js', get_template_directory_uri() . '/assets/js/classcal/classcal-client.js', array('jquery'), 'null', true );
	}
}
add_action( 'wp_enqueue_scripts', 'classcal_client_scripts' );

// Admin Scripts + Styles
function classcal_admin_scripts() {
	global $post_type;
	if( 'studios' == $post_type || 'classcal_classes' == $post_type){
		wp_register_style( 'classcal-admin-css', get_template_directory_uri() . '/assets/css/classcal/classcal-admin.css', false, '1.0.0' );
		wp_enqueue_style( 'classcal-admin-css' );
		wp_enqueue_script( 'classcal-admin-js', get_template_directory_uri() . '/assets/js/classcal/classcal-admin.js', array('jquery'), 'null', true );
	}
}
add_action( 'admin_enqueue_scripts', 'classcal_admin_scripts' );


// Admin redirect
/*function admin_redirects() {
	global $pagenow;

	if($pagenow == 'edit.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'classcal_class'){
		wp_redirect(admin_url('/post-new.php?post_type=page', 'http'), 301);
		exit;
	}
}
add_action('admin_init', 'admin_redirects');*/

function classcal_seating_grid($active_seats = '', $available_seats = ''){
	if (!empty($active_seats)) {
		$active_seats = json_decode(urldecode($active_seats), true); //decode from urlencoded json
	}
	$spot_count = 0;
	$spot_rows = 7;
	$spot_columns = 10;
 /*echo '<pre>';
	var_dump($active_seats);
 echo '</pre>';*/

	echo '<div id="seating-grid" class="classcal-seating-grid">';
	for ($row=1; $row <= $spot_rows; $row++) {
		echo '<div class="classcal-seating-grid-row">';
		for ($col=1; $col <= $spot_columns; $col++) {
			echo '<div class="classcal-seating-grid-cell '.$active_seats[$spot_count]['status'].'" data-spot-row="'.$row.'" data-spot-column="'.$col.'"><div class="classcal-seating-grid-spot"></div></div>';
			$spot_count++;
		}
		echo '</div>';
	}
	echo '</div>';
}

// Metabox for studio seating grid setup
function classcal_studio_seating_metabox() {
	add_meta_box('classcal-studio-seating', 'Edit Seating Layout', 'classcal_studio_seating_metabox_content', 'studios', 'normal', 'default');
}
function classcal_studio_seating_metabox_content() {
	global $post;

	echo '<div class="classcal">';

	echo '<p class="description">Select the active seating/spot layout for this studio. Leave spots blank where you would like a gap to appear (between rows or sections). <b>Important:</b> Adding or removing spots will impact currently active classes and the assigned positions of participants.</p>';

	echo '<div class="classcal-seating-grid-outer"><div class="classcal-instructor"><img src="'.get_template_directory_uri().'/assets/images/classcal-instructor.png"/></div>';

	$studio_seats_comma_list = get_post_meta($post->ID, 'studio_seats_comma_list', true);

	classcal_seating_grid($studio_seats_comma_list);

	echo '<input type="hidden" name="studiometa_noncename" id="studiometa_noncename" value="'.wp_create_nonce('classcal_set_studio_seating').'" /><input type="hidden" name="studio_seats_comma_list" id="studio_seats_comma_list" value="'.$studio_seats_comma_list.'" />';

	echo '</div></div>';

}
function classcal_save_studio_seating_metabox($post_id, $post) {

	if ( !wp_verify_nonce( $_POST['studiometa_noncename'], 'classcal_set_studio_seating' )) {
		return $post->ID;
	}

	if ( !current_user_can( 'edit_post', $post->ID )){
		return $post->ID;
	}

	$studio_meta['studio_seats_comma_list'] = $_POST['studio_seats_comma_list'];

	foreach ($studio_meta as $key => $value) {
		if( $post->post_type == 'revision' ) return;
		$value = implode(',', (array)$value);
		if(get_post_meta($post->ID, $key, FALSE)) {
			update_post_meta($post->ID, $key, $value);
		} else {
			add_post_meta($post->ID, $key, $value);
		}
		if(!$value) delete_post_meta($post->ID, $key);
	}

}

add_action('save_post', 'classcal_save_studio_seating_metabox', 1, 2); // save the custom fields

// Metabox for class seating grid & user list


// Ajax get class seating grid and available spots
function classcal_get_class_seating_grid() {

	/*$slides = get_posts(array(
		post_type => 'community-media',
		posts_per_page => $_POST["amount"],
		post_status => 'publish',
		offset => $_POST["offset"]
	));

	foreach ($slides as $index => $slide) {
		$photo = wp_get_attachment_image_src( get_field('photo_upload', $slide->ID), 'full' );
		$slide->photo_url = $photo[0];
		$md = get_field('media_description', $slide->ID);
		$slide->media_description = ($md) ? $md : '';
		$slide->media_author = get_field('media_author', $slide->ID);
		$slide->media_city = get_field('media_city', $slide->ID);
		$slide->media_state = get_field('media_state', $slide->ID);
	}

	echo json_encode($slides);
	exit;*/
}
add_action( 'wp_ajax_classcal_get_seating_grid', 'classcal_get_seating_grid' );
add_action( 'wp_ajax_nopriv_classcal_get_seating_grid', 'classcal_get_seating_grid' );

// Add classes ajax
function classcal_add_classes() {
	// Create EventParent
	// Create each EventSchedule until end of EventParent range
	// Create Spivi Event for each EventSchedule
}
add_action( 'wp_ajax_classcal_add_classes', 'classcal_add_classes' );

// Update classes ajax
function classcal_update_classes() {
	// Update all
		// Email to each User of future events
	// Update future
		// Email to each User
	// Update individual
		// Add EventParent with text changes
		// Change EventParent ID on this EventSchedule
		// Email to each User
}
add_action( 'wp_ajax_classcal_update_classes', 'classcal_update_classes' );

// Delete classes ajax
function classcal_delete_classes() {
	// Remove Users from Spivi Event
	// Cancel Spivi Event
	// Delete each EventSchedule
	// Email each User
	// Add back credit for each User
	// Set all EventAppointment
}
add_action( 'wp_ajax_classcal_delete_classes', 'classcal_delete_classes' );

// Add user to classes ajax
function classcal_add_to_classes() {
	// Redirect if not logged in (login) or no credits (packages) -> back to calendar
	// Add User to Spivi Event
	// Add User to EventSchedule
	// Create EventAppointment
}
add_action( 'wp_ajax_classcal_add_to_classes', 'classcal_add_to_classes' );
add_action( 'wp_ajax_nopriv_classcal_add_to_classes', 'classcal_add_to_classes' );
