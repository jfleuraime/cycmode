<?php
/*
 * Template Name: Buy Classes Page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<div class="content-header show-header-bg">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>

				<div class="content-body">
					<div id="submenu" class="page-submenu">
						<div class="menu-about-section-submenu-container">
							<?php if( get_field('packages_group') || get_field('membership_group') ) { ?>
							<ul id="menu-about-section-submenu" class="menu">
								<?php if( get_field('packages_group') ) { ?><li><a href="#packages">Packages</a></li><?php } ?>
								<?php if( get_field('membership_group') ) {?><li><a href="#memberships">Commitment Plans</a></li><?php } ?>
								<!--<li><a href="#giftcards">Gift Cards</a></li>-->
							</ul>
							<?php } ?>
						</div>
					</div>
					<p class="featured-text"><?php the_field('buy_description'); ?></p>
					<?php if( get_field('packages_group') ) {
						$loop_count = count(get_field('packages_group')); ?>
						<div id="packages">
							<h2>Packages</h2>
							<hr>
							<div class="buy-class-row">
								<?php while( the_repeater_field('packages_group') ) {
									$package = get_sub_field('package_item');

									if( $package ){
										$post = $package;
										setup_postdata( $post );?>
											<div class="package-item">
												<?php wc_get_template_part( 'content', 'package' ); ?>
											</div>
											<?php wp_reset_postdata();
										}
									} ?>
							</div>
						</div>
					<?php } ?>
					<?php if( get_field('membership_group') ) {
						$loop_count = count(get_field('membership_group')); ?>
						<div id="memberships">
							<h2>Commitment Plans</h2>
							<hr>
							<div class="buy-class-row">
									<?php while( the_repeater_field('membership_group') ) {
										$membership = get_sub_field('membership_item');

										if( $membership ){
											$post = $membership;
											setup_postdata( $post );?>
											<div class="membership-item">
												<?php wc_get_template_part( 'content', 'membership' ); ?>
											</div>
											<?php wp_reset_postdata();
											}
										} ?>
							</div>
						</div>
					<?php } ?>
					<!--<div id="giftcards">
						<h2>Gift Cards</h2>
						<hr>
						<?php
						/*$query = new WP_Query(array('post_type' => 'product', 'product_cat' => 'class-gift-cards'));

						if ( $query->have_posts() ) {
							$loop_count = $query->post_count;*/?>
						<div class="pure-g buy-class-row">
							<?php //while ( $query->have_posts() ) : $query->the_post();?>
							<div class="pure-u-1-1 pure-u-lg-1-<?php //echo $loop_count; ?>">
								<?php //wc_get_template_part( 'content', 'giftcard' ); ?>
							</div>
							<?php //endwhile;?>
						</div>
						<?php /*} else {
							echo __( 'No Class Gift Cards found' );
						}
						wp_reset_postdata();*/
						?>
					</div>-->
					<?php if (get_field('buy_fine_print')) { ?>
						 <div class="fine-print"><?php the_field('buy_fine_print'); ?></div>
					 <?php } ?>
				</div>

				<?php endwhile; ?>

		</main>
	</div>
	<?php
get_footer();
