<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="tml tml-login" id="theme-my-login<?php $template->the_instance(); ?>">
  <?php if (!is_page_template('page-auth.php')) {?>
  <h3>Log In</h3>
  <?php }?>
  <?php $template->the_action_template_message( 'login' ); ?>
  <?php $template->the_errors(); ?>
  <form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login', 'login_post' ); ?>" method="post">
    <div class="tml-user-login-wrap">
      <label for="user_login<?php $template->the_instance(); ?>"><?php
        if ( 'username' == $theme_my_login->get_option( 'login_type' ) ) {
          _e( 'Username', 'theme-my-login' );
        } elseif ( 'email' == $theme_my_login->get_option( 'login_type' ) ) {
          _e( 'E-mail', 'theme-my-login' );
        } else {
          _e( 'Username or E-mail', 'theme-my-login' );
        }
      ?></label>
      <input type="text" name="log" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'log' ); ?>" placeholder="Username or Email Address" size="20" />
    </div>

    <div class="tml-user-pass-wrap">
      <label for="user_pass<?php $template->the_instance(); ?>"><?php _e( 'Password', 'theme-my-login' ); ?></label>
      <input type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" class="input" value="" size="20" autocomplete="off" placeholder="Password" />
    </div>
    <a href="<?php echo home_url('/lostpassword') ?>" rel="nofollow">Forgot your password?</a>

    <?php do_action( 'login_form' ); ?>

    <div class="tml-rememberme-submit-wrap">
      <div class="tml-rememberme-wrap">
        <input name="rememberme" type="checkbox" id="rememberme<?php $template->the_instance(); ?>" value="forever" />
        <label for="rememberme<?php $template->the_instance(); ?>"><?php esc_attr_e( 'Remember Me', 'theme-my-login' ); ?></label>
      </div>

      <div class="tml-submit-wrap">
        <input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Log In', 'theme-my-login' ); ?>" />
        <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
        <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
        <input type="hidden" name="action" value="login" />
      </div>
    </div>
  </form>
  <div>
    <span>New member?</span>
    <?php if (!is_page_template('page-auth.php')) {?>
    <a href="#" class="open-register" rel="nofollow">Join Now!</a>
    <?php } else {?>
    <a href="<?php echo home_url('/register'); ?>" rel="nofollow">Join Now!</a>
    <?php } ?>
  </div>
</div>
