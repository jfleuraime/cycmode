<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );
$current_user_id = get_current_user_id();
$profile_url = bp_loggedin_user_domain();
?>
<div id="content-member-header" role="complementary">
	<div id="item-header-avatar">
		<a href="<?php echo $profile_url; ?>profile/">
			<?php echo get_avatar($current_user_id); ?>
		</a>
	</div><!-- #item-header-avatar -->

	<div id="item-header-content">
		<h2><?php echo bp_core_get_user_displayname( $current_user_id ) ?></h2>
	</div><!-- #item-header-content -->

</div><!-- #item-header -->
<div id="content-member-nav">
	<div class="item-list-tabs no-ajax" id="object-nav" role="navigation">
		<ul>
			<li id="xprofile-personal-li">
				<a id="user-xprofile" href="<?php echo $profile_url; ?>profile/">Profile</a>
			</li>
			<li id="classes-personal-li">
				<a id="user-classes" href="<?php echo $profile_url; ?>classes/">Classes</a>
			</li>
			<li id="settings-personal-li">
				<a id="user-settings" href="<?php echo $profile_url; ?>settings/">Settings</a>
			</li>
			<li class="current selected">
				<a href="#">Purchases</a>
			</li>
		</ul>
	</div>
</div>
<div id="content-member-body">
	<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
	<ul>
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
			<li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
				<a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
	</div>


<?php do_action( 'woocommerce_after_account_navigation' ); ?>
