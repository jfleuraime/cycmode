<?php
/**
 * The template for displaying package content within loops
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$product_id = $product->get_id();
$price = $product->get_price();
$detail = get_field('gift_card_detail');
?>
<div id="giftcard-<?php echo $product_id;?>" class="product-media product-giftcard">
	<div class="product-media-top">
		<span>Gift Card Value</span>
		<h1>$<?php echo $price;?></h1>
		<span><i><?php echo $detail;?></i></span>
	</div>
	<div class="product-media-bottom">
		<a href="<?php echo home_url('/cart');?>/?add-to-cart=<?php echo $product_id;?>" class="button button-wht">Select</a>
	</div>
</div>
