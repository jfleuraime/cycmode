<?php
/**
 * The template for displaying membership content within loops
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$product_id = $product->get_id();
$price = $product->get_price();
$period = WC_Subscriptions_Product::get_period( $product );
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'large' );
$membership_name = get_field('membership_name');
if ($period == 'day') {
	$period_formated = 'dy';
} elseif ($period == 'week') {
	$period_formated = 'wk';
} elseif ($period == 'month') {
	$period_formated = 'mo';
} else {
	$period_formated = 'yr';
}
?>
<div id="membership-<?php echo $product_id;?>" class="product-media product-membership">
	<!--<div class="product-media-image" style="background: url(<?php //echo $image[0]; ?>) no-repeat center center; background-size: cover"></div>-->
	<div class="product-media-top">
		<h4><?php echo $membership_name; ?></h4>
		<?php if( get_field('membership_items') ) { ?>
		<ul>
			<?php while( the_repeater_field('membership_items') ) { ?>
				<li><?php the_sub_field('membership_item'); ?></li>
			<?php } ?>
		</ul>
		<?php } ?>
	</div>
	<div class="product-media-bottom">
		<span>$<?php echo $price;?>/<?php echo $period_formated; ?></span>
		<a href="<?php echo home_url('/cart');?>/?add-to-cart=<?php echo $product_id;?>" class="button button-wht">Select</a>
	</div>
</div>
