<?php
/**
 * The template for displaying package content within loops
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
$product_id = $product->get_id();
$price = $product->get_price();
$number_classes = get_field('package_classes');
$expires_in = get_field('package_expiration');
$price_per_class = get_field('package_class_cost');
?>
	<div id="package-<?php echo $product_id;?>" class="product-media product-package">
		<div class="product-media-top">
			<h3><?php echo $number_classes; if ($number_classes > 1) { echo ' Classes';} else { echo ' Class';}?></h3>
			<div>Each Class Only <?php echo $price_per_class;?></div>
			<span><i>Expires in <?php echo $expires_in;?></i></span>
		</div>
		<div class="product-media-bottom">
			<span>$<?php echo $price;?></span>
			<a href="<?php echo home_url('/cart');?>/?add-to-cart=<?php echo $product_id;?>" class="button button-wht">Select</a>
		</div>
	</div>
