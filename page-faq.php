<?php
/**
 * Template Name: FAQ Page
 */

get_header();
function toUrl($string) {
	//Lower case everything
	$string = strtolower($string);
	//Make alphanumeric (removes all other characters)
	$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	//Clean up multiple dashes or whitespaces
	$string = preg_replace("/[\s-]+/", " ", $string);
	//Convert whitespaces and underscore to dash
	$string = preg_replace("/[\s_]/", "-", $string);
	return $string;
} ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-header show-header-bg">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>

		<div class="content-body">
			<?php if( have_rows('faq_section') ) { ?>
				<div id="submenu" class="page-submenu">
					<div class="menu-about-section-submenu-container">
						<ul id="menu-about-section-submenu" class="menu">
							<?php while( have_rows('faq_section') ): the_row(); ?>
							<li><a href="#<?php echo toUrl(get_sub_field('faq_section_title')); ?>"><?php the_sub_field('faq_section_title'); ?></a></li>
							<?php endwhile; ?>
						</ul>
					</div>
				</div>
			<?php } ?>
			<?php if( have_rows('faq_section') ) {
				while( have_rows('faq_section') ): the_row(); ?>
					<div class="faq-section" id="<?php echo toUrl(get_sub_field('faq_section_title')); ?>">
						<h2><?php the_sub_field('faq_section_title'); ?></h2>
						<?php if( have_rows('faq_qa') ) { ?>
						<ul class="faq-list">
							<?php while( have_rows('faq_qa') ): the_row(); ?>
							<li class="faq-question"><?php the_sub_field('faq_question'); ?></li>
							<li class="faq-answer"><?php the_sub_field('faq_answer'); ?></li>
							<?php endwhile; ?>
						</ul>
						<?php } ?>
					</div>
				<?php endwhile;
			} ?>
		</div>


		<?php endwhile; ?>

	</main>
</div>
<?php
get_footer();
