<?php
/**
 * Template Name: Home Page
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <?php while ( have_posts() ) : the_post(); ?>

      <?php if ( have_rows( 'slides' ) ) : ?>

          <ul id="home-slider" class="slider">
            <?php while ( have_rows( 'slides' ) ) : the_row();
            $image = get_sub_field( 'slide_image' );
            $headline = get_sub_field( 'slide_headline' );
            $layout = get_sub_field( 'slide_headline_layout' );
            ?>
            <li class="slide">
              <div class="slide-inner" style="background:url(<?php echo $image; ?>)">
               <div class="slide-inner-content <?php echo $layout; ?>">
                <h2><?php echo $headline; ?></h2>
                 <?php if ( have_rows( 'slide_buttons' ) ) : ?>
                  <div class="button-wrap">
                    <?php while ( have_rows( 'slide_buttons' ) ) : the_row();
                    $button_text = get_sub_field( 'button_text' );
                    $button_link = get_sub_field( 'button_link' );
                    $button_color = get_sub_field( 'button_color' );
                    $button_style = get_sub_field( 'button_style' );
                    if ($button_color == 'black') {
                      if ($button_style == 'outline') {
                        $button_css = 'button-blk';
                      } else {
                        $button_css = 'button-blk-filled';
                      }

                    } elseif ($button_color == 'white') {
                      if ($button_style == 'outline') {
                        $button_css = 'button-wht';
                      } else {
                        $button_css = 'button-wht-filled';
                      }
                    } else {
                      if ($button_style == 'outline') {
                        $button_css = 'button-alt';
                      }
                    }
                    ?>
                    <a href="<?php echo $button_link; ?>" class="button <?php echo $button_css; ?>"><?php echo $button_text; ?></a>
                  <?php endwhile; ?>
                 </div>
                <?php endif; ?>
                </div>
              </div>
            </li>
            <?php endwhile; ?>
          </ul>

        <?php endif; ?>

      <?php endwhile; ?>

    </main>
  </div>
  <?php
get_footer();
