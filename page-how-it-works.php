<?php
/**
 * Template Name: How it Works Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post();
		$page_layout = get_field('page_layout');
		$show_header_bg = get_field('show_header_bg');
		$show_submenu = get_field('show_submenu');
		$select_submenu = get_field('select_submenu'); ?>

		<div class="content-header <?php echo $show_header_bg; ?>">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>

		<div class="content-body <?php echo $page_layout; ?>">
			<?php if ($show_submenu == 'submenu-visible' && $select_submenu) {
	get_template_part( 'components/header/navigation', 'submenu' );
} ?>
			<p class="featured-text"><?php the_field('hiw_description'); ?></p>
			<?php if( get_field('hiw_steps') ) { ?>
			<div class="pure-g steps">
				<?php while( the_repeater_field('hiw_steps') ) { ?>
				<div class="pure-u-1-1 pure-u-lg-1-<?php echo count( get_field('hiw_steps') ); ?>">
					<div class="step">
						<div class="step-icon"><img src="<?php the_sub_field('step_icon'); ?>" alt=""></div>
						<h2><?php the_sub_field('step_name'); ?></h2>
						<p><?php the_sub_field('step_description'); ?></p>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } ?>
			<a href="#" class="button">View Classes</a>
		</div>


		<?php endwhile; ?>

	</main>
</div>
<?php
get_footer();
