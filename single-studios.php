<?php
/**
 * The template for displaying all single studios.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cycmode
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>
		<div class="content-body">
			<?php $post_type = get_post_type();
			get_template_part( 'components/post/content', 'studios' ); ?>
		</div>
		<?php endwhile; // End of the loop.
		?>

	</main>
</div>
<?php
get_footer();


