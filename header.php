<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cycmode
 */

?>
  <!DOCTYPE html>
  <html <?php language_attributes(); ?>>

  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <?php get_template_part( 'components/header/navigation', 'mobile' ); ?>

    <div id="page" class="site">

      <a class="skip-link screen-reader-text" href="#content">
        <?php esc_html_e( 'Skip to content', 'cycmode' ); ?>
      </a>

      <header id="masthead" class="site-header" role="banner">
        <div class="pure-g header-wrap">
          <div class="pure-u-1-1 pure-u-lg-1-4">
            <?php get_template_part( 'components/header/navigation', 'logo' ); ?>
          </div>
          <div class="pure-u-1-1 pure-u-lg-3-4">
            <?php get_template_part( 'components/header/navigation', 'top' ); ?>
          </div>
        </div>

      </header>
      <div id="content" class="site-content">
