<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package cycmode
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="content-header">
				<h1>404</h1>
			</div>

			<div class="content-body">
				<h3>Not Found</h3>
				<p>The page you are looking for no longer exists. Perhaps you can return back to the site’s homepage and see if you can find what you are looking for.</p>
			</div>

		</main>
	</div>
	<?php
get_footer();
