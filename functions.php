<?php
/**
 * cycmode functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cycmode
 */

if ( ! function_exists( 'cycmode_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function cycmode_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on components, use a find and replace
	 * to change 'cycmode' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'cycmode', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'cycmode-featured-image', 640, 9999 );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'top' => esc_html__( 'Header Menu', 'cycmode' ),
		'footer' => esc_html__( 'Footer Menu', 'cycmode' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'cycmode_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'cycmode_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cycmode_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cycmode_content_width', 640 );
}
add_action( 'after_setup_theme', 'cycmode_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cycmode_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'cycmode' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'cycmode_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cycmode_scripts() {
	wp_enqueue_script( 'smartresize', get_template_directory_uri() . '/assets/js/smartresize.js', array('jquery'), 'null', true );
	// Add slider script
	wp_enqueue_style( 'slick-css', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
	wp_enqueue_script( 'slick-js', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array('jquery'), 'null', true );
	if (is_page_template('page-home.php')) {
		wp_enqueue_script( 'home-js', get_template_directory_uri() . '/assets/js/home.js', array('jquery'), 'null', true );
	}
	wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700|Oswald:300,400' );
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
	wp_enqueue_style( 'cycmode-style', get_template_directory_uri() . '/assets/css/style.css');

	wp_enqueue_script( 'cycmode-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array('jquery'), 'null', true );
	wp_enqueue_script( 'slideout', get_template_directory_uri() . '/assets/js/slideout.min.js', array('jquery'), 'null', true );
	wp_enqueue_script( 'cycmode-js', get_template_directory_uri() . '/assets/js/site.js', array('jquery'), 'null', true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'cycmode_scripts' );

function load_custom_wp_admin_style() {
	wp_register_style( 'cycmode-admin', get_template_directory_uri() . '/assets/css/cycmode-admin.css', false, '1.0.0' );
	wp_enqueue_style( 'cycmode-admin' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Spivi Ajax Integration.
 */
//require get_template_directory() . '/inc/spivi.php';

/**
 * Load CMB2 Seating Chart Integration.
 */
//require get_template_directory() . '/inc/seating-charts.php';

/**
 * Load ClassCal Schedule.
 */
//require get_template_directory() . '/inc/classcal.php';


// Add custom post types
add_action( 'init', 'cycmode_cpt' );
function cycmode_cpt() {
	$labels = array(
		"name" => __( 'Classes', 'cycmode' ),
		"singular_name" => __( 'Class', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Classes', 'cycmode' ),
		"labels" => $labels,
		"menu_icon" => "dashicons-welcome-learn-more",
		"description" => "",
		"public" => true,
		"show_ui" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "classes", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "classes", $args );

	$labels = array(
		"name" => __( 'Studios', 'cycmode' ),
		"singular_name" => __( 'Studio', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Studios', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-location-alt",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "studios", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title", "editor" ),
	);
	register_post_type( "studios", $args );

	$labels = array(
		"name" => __( 'Instructors', 'cycmode' ),
		"singular_name" => __( 'Instructor', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Instructors', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-admin-users",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "instructors", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "instructors", $args );

	$labels = array(
		"name" => __( 'Careers', 'cycmode' ),
		"singular_name" => __( 'Career Listing', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Careers', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-businessman",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "careers", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title", "editor" ),
	);
	register_post_type( "careers", $args );

	$labels = array(
		"name" => __( 'Press', 'cycmode' ),
		"singular_name" => __( 'Press', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Press', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-format-aside",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "press", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "press", $args );

	$labels = array(
		"name" => __( 'Community Content', 'cycmode' ),
		"singular_name" => __( 'Community Content', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Community', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"menu_icon" => "dashicons-groups",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "community", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title", "editor" ),
	);
	register_post_type( "community", $args );

	$labels = array(
		"name" => __( 'Seating Charts', 'cycmode' ),
		"singular_name" => __( 'Seating Chart', 'cycmode' ),
	);

	$args = array(
		"label" => __( 'Seating Charts', 'cycmode' ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"show_ui" => true,
		"menu_icon" => "dashicons-groups",
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "seating-chart", "with_front" => true ),
		"query_var" => true,

		"supports" => array( "title" ),
	);
	register_post_type( "seating-chart", $args );

}


// Redirect wp-login to custom login page
function redirect_login_page(){
	$current_page = basename( $_SERVER['REQUEST_URI'] );

	if( $current_page == "wp-login.php" ) {
		wp_redirect( home_url('/login') );
		exit();
	}
}
add_action( 'init','redirect_login_page' );

//Page Redirects based on auth and role
function page_redirect(){
	global $bp;

	if (is_user_logged_in()) {
		// Send user to home if on auth related page
		if (is_page( 'resetpass' ) || is_page( 'login' ) || is_page( 'lostpassword' ) || is_page( 'register-success' )) {
			wp_redirect( home_url() );
			exit();
		}
	}
	if (!is_user_logged_in() && (is_checkout())) {
		// Send visitors to login before viewing products
		wp_redirect( wp_login_url( get_permalink() ));
		exit();
	}
	// Prevent viewing other profiles
	if ( in_array($bp->current_component, array('members','profile')) ) {
		if (bp_displayed_user_id() != get_current_user_id()) {
			if (is_user_logged_in()) {
				wp_redirect( bp_core_get_user_domain(get_current_user_id()));
				exit;
			} else {
				wp_redirect( home_url('/login') );
				exit();
			}
		}
	}
}
add_action('template_redirect', 'page_redirect');

// Add placeholder xprofile fields
function wp_add_placeholder($elements){
	$elements['placeholder'] = bp_get_the_profile_field_name();
	return $elements;
}
add_action('bp_xprofile_field_edit_html_elements','wp_add_placeholder');

// Redirect BuddyPress additional register forms to original page on submit
function cycmode_redirect_on_signup() {
	$referrer = $_SERVER['HTTP_REFERER'];

	if ( 'POST' !== strtoupper( $_SERVER['REQUEST_METHOD'] ) ){
		return;
	}

	$bp = buddypress();

	if( ! empty( $bp->signup ) ) {

		$_SESSION['cycmode_signup'] = $bp->signup;
		$_SESSION['cycmode_signup_fields'] = $_POST;
	}

	bp_core_redirect($referrer);
}

add_action('bp_core_screen_signup', 'cycmode_redirect_on_signup');

// Auto activate users
/*function cycmode_signup_form_validation_text() {
	return false;
}

add_filter( 'bp_registration_needs_activation'	, 'cycmode_signup_form_validation_text');*/

// Process signup errors for additional BuddyPress registration forms
function cycmode_process_signup_errors(){

	if (is_user_logged_in()) {
		return;
	}

	if (!session_id()) {
		session_start();
	}

	if (!empty($_SESSION['cycmode_signup'])) {

		$bp = buddypress();
		$bp->signup = $_SESSION['cycmode_signup'];

		if (isset($bp->signup->errors) && !empty($bp->signup->errors)) {
			$_POST = $_SESSION['cycmode_signup_fields'];
			$_POST['open_slidedown'] = 'open_register_tab';
		}

		$errors = array();

		if (isset($bp->signup->errors)) {
			$errors = $bp->signup->errors;
		}

		foreach ((array) $errors as $fieldname => $error_message) {

			add_action('bp_' . $fieldname . '_errors', create_function('', 'echo apply_filters(\'bp_members_signup_error_message\', "<div class=\"error\">" . stripslashes( \'' . addslashes($error_message) . '\' ) . "</div>" );'));
		}

		$_SESSION['cycmode_signup'] = null;
		$_SESSION['cycmode_signup_fields'] = null;
	}
}

add_action( 'bp_init', 'cycmode_process_signup_errors' );

// Add terms and conditions checkbo to register
function cycmode_add_to_registration() {
	global $bp;
?>
<div class="form-group-wide">
	<?php do_action('bp_accept_tos_errors') ?>
	<input id="accept_tos" name="accept_tos" type="checkbox" value="agreed">
	<label for="accept_tos" class="accept_tos">I accept the <a href='<?php echo home_url(); ?>/terms-conditions'>terms & conditions</a></label>
</div>
<?php
}
add_action('bp_before_registration_submit_buttons', 'cycmode_add_to_registration', 36);

function cycmode_validate_user_registration() {
	global $bp;

	if (!isset($_POST['accept_tos']) || empty($_POST['accept_tos']) || $_POST['accept_tos'] != 'agreed') {
		$bp->signup->errors['accept_tos'] = __('Please accept the Terms and Conditions', 'buddypress');
	}
	return;
}

add_action('bp_signup_validate', 'cycmode_validate_user_registration');

// Remove form names from TML
function tml_title( $title, $action ) {
	$title = '';
	return $title;
}
add_filter( 'tml_title', 'tml_title', 11, 2 );

// Add body class on login/error
function cyc_body_classes($classes) {
	if (!is_page_template('page-auth.php') && $_POST && isset($_POST['open_slidedown']) && $_POST['open_slidedown'] === 'open_register_tab') {
		$classes[] = 'slidedown';
		$classes[] = 'slidedown-error';
	}
	// return the $classes array
	return $classes;
}
add_filter( 'body_class','cyc_body_classes' );

// Change lost password link url
function reset_pass_url() {
	$siteURL = get_option('siteurl');
	return "{$siteURL}/lostpassword";
}
add_filter( 'lostpassword_url',  'reset_pass_url', 11, 0 );


// Add classes profile page
function bp_custom_user_nav_item() {
	global $bp;

	$args = array(
		'name' => __('Classes', 'buddypress'),
		'slug' => 'classes',
		'default_subnav_slug' => 'classes',
		'position' => 50,
		'screen_function' => 'bp_custom_user_nav_item_screen',
		'item_css_id' => 'classes'
	);

	bp_core_new_nav_item( $args );
}
add_action( 'bp_setup_nav', 'bp_custom_user_nav_item', 99 );

// the calback function from our nav item arguments

function bp_custom_user_nav_item_screen() {
	add_action( 'bp_template_content', 'bp_custom_screen_content' );
	bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

// the function hooked to bp_template_content, this hook is in plugns.php

function bp_custom_screen_content() {

	echo '<p>You do not have any active classes.</p>';
}

add_action( 'bp_member_options_nav', 'buddydev_add_profile_extra_custom_links' );

function buddydev_add_profile_extra_custom_links() { ?>
<li><a href="<?php echo home_url(); ?>/my-account/orders">Purchases</a></li>
<?php }


// Remove product short description
function remove_short_description() {
	remove_meta_box( 'postexcerpt', 'product', 'normal');
}

add_action('add_meta_boxes', 'remove_short_description', 999);

// Remove Order Notes from checkout field in Woocommerce
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
function alter_woocommerce_checkout_fields( $fields ) {
	unset($fields['order']['order_comments']);
	unset($fields['billing']['billing_company']);
	return $fields;
}

// Make phone number optional
add_filter( 'woocommerce_billing_fields', 'wc_npr_filter_phone', 10, 1 );
function wc_npr_filter_phone( $address_fields ) {
	$address_fields['billing_phone']['required'] = false;
	return $address_fields;
}

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');

//remove admin notice for ConvertPlug
define('BSF_14058953_NOTICES', false);
remove_action('admin_notices','bsf_notices',1050);

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'general-settings',
		'capability'	=> 'manage_options',
		'position' => 1,
		'redirect'		=> false
	));
}

// Redirect to confirmation page after signup
function bp_redirect($user) {

	$redirect_url = home_url('/register-success');

	bp_core_redirect($redirect_url);

}

add_action('bp_core_signup_user', 'bp_redirect', 100, 1);

// Pre-populate first and last name on checkout page
add_filter('woocommerce_checkout_get_value', function($input, $key ) {
	global $current_user;
	$current_user_id = $current_user->ID;
	$first_name = bp_get_profile_field_data(array('field' => 'First Name', 'user_id' => $current_user_id));
	$last_name = bp_get_profile_field_data(array('field' => 'Last Name', 'user_id' => $current_user_id));

	switch ($key) :

		case 'billing_first_name':
			return $first_name;
		break;

		case 'billing_last_name':
			return $last_name;
		break;

	endswitch;
}, 10, 2);

// Add new signups to MyEmma
function my_emma_add_to_list($user_email, $first_name = '', $last_name = '') {

	$account_id = get_field("myemma_account_id", "option");
	$public_api_key = get_field("myemma_public_api_key", "option");
	$private_api_key = get_field("myemma_private_api_key", "option");
	$url = "https://api.e2ma.net/".$account_id."/members/add";

	if (!$account_id || !$public_api_key || !$private_api_key || !$user_email) {
		return false;
	}

	$post_data = array(
		"email" => $user_email,
		"fields" => array(
			"first_name" => $first_name,
			"last_name" => $last_name
		)
	);

	// Post to MyEmma
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_USERPWD, $public_api_key . ":" . $private_api_key);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, count($post_data));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

}

// Add new signups to GiftBit - return contactID
function giftbit_add_contact($user_id, $user_email) {

	$gitbit_access_token = get_field("gitbit_access_token", "option");
	$url = 'https://currencyapi.giftbit.com/v1/contacts';

	if (!$user_id || !$user_email || !$gitbit_access_token) {
		return false;
	}

	$user_supplied_id = 'transaction_'. $user_id . '_' . uniqid();

	// Post to GiftBit
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_POST, TRUE);

	curl_setopt($ch, CURLOPT_POSTFIELDS, "{
	  \"userSuppliedId\": \"".$user_supplied_id."\",
	  \"email\": \"".$user_email."\"
	}");

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json",
		"Authorization: Bearer ".$gitbit_access_token.""
	));

	$response = curl_exec($ch);
	curl_close($ch);

	$data = json_decode($response);

	update_user_meta($user_id, 'contact_id', $data['contact']['contactId']);

	return $data['contact']['contactId'];
}

// Add card to GiftBit
function giftbit_add_card($user_id, $contact_id) {
	$gitbit_access_token = get_field("gitbit_access_token", "option");
	$url = 'https://currencyapi.giftbit.com/v1/cards/';

	if (!$contact_id || $user_id || !$gitbit_access_token) {
		return false;
	}

	$user_supplied_id = 'transaction_'. $user_id . '_' . uniqid();

	// Post to GiftBit
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_POST, TRUE);

	curl_setopt($ch, CURLOPT_POSTFIELDS, "{
	  \"userSuppliedId\": \"".$user_supplied_id."\",
	  \"code\": {
		\"initialValue\": 0,
		\"currency\": \"XXX\"
	  },
	  \"contactId\": \"".$contact_id."\"
	}");

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json",
		"Authorization: Bearer ".$gitbit_access_token.""
	));

	$response = curl_exec($ch);
	curl_close($ch);

	$data = json_decode($response);

	update_user_meta($user_id, 'card_id', $data['cardId']);
}

// Add value to GiftBit card
function giftbit_card_transaction($user_id, $card_id, $value = '') {

	$gitbit_access_token = get_field("gitbit_access_token", "option");
	if (!$user_id || !$card_id || !$value || !$gitbit_access_token) {
		return false;
	}
	$url = 'https://currencyapi.giftbit.com/v1/cards/'.$card_id.'/code/transactions';

	$user_supplied_id = 'transaction_'. $card_id . '_' . uniqid();

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_POST, TRUE);

	curl_setopt($ch, CURLOPT_POSTFIELDS, "{
	  \"value\": ".$value.",
	  \"currency\": \"XXX\",
	  \"userSuppliedId\": \"".$user_supplied_id."\"
	}");

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json",
		"Authorization: Bearer ".$gitbit_access_token.""
	));

	$response = curl_exec($ch);
	curl_close($ch);

	$data = json_decode($response);

	update_user_meta($user_id, 'point_balance', $data['transaction']['valueAvailableAfterTransaction']);
}

// Get GiftBit card balance
function giftbit_get_balance($user_id) {
	$gitbit_access_token = get_field("gitbit_access_token", "option");
	if (!$user_id || !$gitbit_access_token) {
		return false;
	}
	$url = 'https://currencyapi.giftbit.com/v1/cards/'.$card_id.'/code/balance';

	$user_supplied_id = 'transaction_'. $user_id . '_' . uniqid();

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);

	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		"Content-Type: application/json",
		"Authorization: Bearer ".$gitbit_access_token.""
	));

	$response = curl_exec($ch);
	curl_close($ch);

	$data = json_decode($response);

	return $data['balance']['currentValue'];
}

// CYCMODE - Third Party Integrations on User Signup
function cyc_user_signup($user_id, $user_login, $user_password, $user_email) {
	$user = get_userdata( $user_id );

	if (!$user) {
		return false;
	}

	// Add to My Emma
	my_emma_add_to_list($user->user_email, $user->first_name, $user->last_name);

	// Add to GiftBit
	$contact_id = giftbit_add_contact($user->ID, $user->user_email);

	// Associate user with GiftBit card
	giftbit_add_card($user->ID, $contact_id);

	// Set class balance to 0
	update_user_meta($user_id, 'class_balance', 0);
}
add_action('bp_core_signup_user', 'cyc_user_signup', 10, 4);

// Checkout - Update Class Balance
function checkout_update_class_balance($order_id) {
	$order = new WC_Order( $order_id );
	$items = $order->get_items();
	$user_id = $order->user_id;

	if (!$user_id) {
		return false;
	}

	$current_balance = get_user_meta($user_id, 'class_balance', true);
	$current_balance = (int) $current_balance;

	foreach ( $items as $item ) {
		$product_id = $item['product_id'];
		$classes_to_add = 0;
		if (get_field('product_type', $product_id) == 'package') {
			$classes_to_add = get_field('package_classes', $product_id);
		} else if (get_field('product_type', $product_id) == 'membership') {
			$classes_to_add = get_field('membership_classes', $product_id);
		}
		if ($classes_to_add > 0) {
			$new_balance = $current_balance + $classes_to_add;
			update_user_meta($user_id, 'class_balance', $new_balance);
		}
	}
}
add_action('woocommerce_checkout_order_processed', 'checkout_update_class_balance', 1, 1);

// Membership Renewal - Update Class Balance
function renewal_update_class_balance($subscription){
	$items = $subscription->get_items();
	$user_id = $subscription->user_id;

	if (!$user_id) {
		return false;
	}

	$current_balance = get_user_meta($user_id, 'class_balance', true);
	$current_balance = (int) $current_balance;

	foreach ( $items as $item ) {
		$product_id = $item['product_id'];
		$classes_to_add = 0;
		if (get_field('product_type', $product_id) == 'membership') {
			$classes_to_add = get_field('membership_classes', $product_id);
			$classes_to_add = (int) $classes_to_add;
		}
		if ($classes_to_add > 0) {
			$new_balance = $current_balance + $classes_to_add;
			update_user_meta($user_id, 'class_balance', $new_balance);
		}
	}
}
add_action('woocommerce_subscription_renewal_payment_complete', 'renewal_update_class_balance', 10, 1);
