<div class="content-loop">
  <h2 class="content-title"><?php the_title(); ?><span> - <?php the_field('news_date'); ?></span></h2>
  <span class="content-subtitle"><?php the_field('news_title'); ?></span>
