<div class="content-instructor">
  <div class="content-instructor-inner">
    <a href="<?php the_permalink(); ?>">
      <img src="<?php the_field('instructor_image'); ?>" alt="">
      <?php the_title( '<h3 class="content-title">', '</h3>' );?>
    </a>
  </div>
</div>
