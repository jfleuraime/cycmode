<?php $post_type = get_post_type();
if ( $post_type == 'classes') { ?>
<p><?php the_field('class_description'); ?></p>
  <a href="#" class="button button-blk">View Times</a>
<?php } elseif ($post_type == 'careers') { ?>
<p><?php echo wp_trim_words(get_the_content(), 65, "..."); ?></p>
<a href="<?php the_permalink(); ?>" class="button button-blk">View Details</a>
<?php } elseif ($post_type == 'press') { ?>
<p><?php the_field('news_description'); ?></p>
<a href="<?php the_field('news_link'); ?>" target="_blank" class="button button-blk">Learn More</a>
<?php } ?>

</div>
