

<?php $content_type = get_field('content_type'); ?>
<div class="content-community">
	<div class="content-community-inner <?php echo $content_type; ?>">
		<?php if ($content_type == 'instagram') {?>
		<a href="<?php echo the_field('instagram_link'); ?>" target="_blank">
		<?php } else {?>
		<a href="<?php the_permalink(); ?>">
			<div class="community-content-caption">
				<?php the_title( '<h3 class="content-title">', '</h3>' ); ?>
			</div>
		<?php }?>
			<img src="<?php the_field('featured_image'); ?>" alt="">
		</a>
	</div>
</div>
