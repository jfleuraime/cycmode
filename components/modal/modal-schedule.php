<div id="reserve-modal" class="modalDialog">
  <div class="reserve-modal-inner">
    <a href="#close" title="Close" class="close">X</a>
    <div class="reserve-modal-header">
      <span>Class Name with <span>John Doe</span></span>
      <h3>Monday, Aug 26 @ 6:30PM</h3>
    </div>
    <div class="reserve-modal-body">
      <div class="reserve-modal-left">
        <button type="button">Reserve</button>
      </div>
      <div class="reserve-modal-right">
        <h4>About this Class</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus varius est quis dictum rhoncus. Ut accumsan elementum vestibulum. Mauris at pellentesque nisi.</p>
      </div>
    </div>
  </div>
</div>
