<?php if ($_POST && isset($_POST['open_slidedown']) && $_POST['open_slidedown'] === 'open_register_tab') { $open_register_tab = true; } else {$open_register_tab = false; }?>
<div class="content-auth-wrap <?php echo ($open_register_tab ? 'content-auth-wrap-open' : ''); ?>">
	<div class="content-auth-inner">
		<div class="content-auth-close close-slidedown"><i class="fa fa-times" aria-hidden="true"></i></div>
		<div class="content-auth-tab login-tab">
			<?php echo do_shortcode("[theme-my-login instance='1']"); ?>
		</div>
		<div class="content-auth-tab register-tab <?php echo ($open_register_tab ? 'open' : ''); ?>">
			<?php $bp = buddypress();

			if( empty( $bp->signup->step ) ){
				$bp->signup->step='request-details';
			}
			get_template_part( 'buddypress/members/register' );?>
		</div>
	</div>
</div>
