<div class="bottom-menu">
	<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_id' => 'footer-menu' )); ?>
	<div class="follow-wrap">
		<a href="<?php the_field('facebook_page_url', 'option');?>" title="Follow cycMODE on Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
		<a href="<?php the_field('twitter_page_url', 'option');?>" title="Follow cycMODE on Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
		<a href="<?php the_field('instagram_page_url', 'option');?>" title="Follow cycMODE on Instagram" target="_blank"><i class="fa fa-instagram"></i></a>
	</div>
</div>
