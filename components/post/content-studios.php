<div class="content-main-wrap">
	<?php if (get_field('enable_sh') == 'yes') {?>
		<div class="staggered-headline">
			<div class="left-headline"><?php the_field('left_staggered_text');?></div>
			<div class="right-headline"><?php the_field('right_staggered_text');?></div>
		</div>
	<?php } ?>
	<div class="content-studio">
		<?php the_content(); ?>
	</div>
</div>
