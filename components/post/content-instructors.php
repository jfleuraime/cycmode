<div class="content-main-right">
	<h1 class="entry-title"><?php the_title();?></h1>
	<p class="intro">"<?php the_field('instructor_featured_text'); ?>"</p>
	<p><?php the_field('instructor_bio'); ?></p>
	<hr>
	<div class="instructor-info-wrap">
		<div class="instructor-info">
			<span>Hometown</span>
			<h3><?php the_field('instructor_hometown'); ?></h3>
		</div>
		<div class="instructor-info">
			<span>Favorite Artist</span>
			<h3><?php the_field('instructor_artist'); ?></h3>
		</div>
		<div class="instructor-info">
			<span>Favorite Bike Move</span>
			<h3><?php the_field('instructor_move'); ?></h3>
		</div>
		<div class="instructor-info">
			<span>Workout Stye</span>
			<h3><?php the_field('instructor_style'); ?></h3>
		</div>
	</div>
	<hr>
	<?php if( get_field('current_tracks') ) { ?>
	<div class="instructor-tracks-wrap">
		<span>What I Currently Playing</span>
		<div class="instructor-tracks-outer">
			<div id="mini-carousel" class="instructor-tracks-inner slider carousel">
				<?php while( the_repeater_field('current_tracks') ) { ?>
				<div class="instructor-track">
					<div class="instructor-track-inner">
					<?php if (get_sub_field('current_track_link')) { ?>
						<a href="<?php the_sub_field('current_track_link'); ?>" target="_blank"><img src="<?php the_sub_field('current_track_image'); ?>" alt="<?php the_sub_field('current_track_name'); ?>"></a>
					<?php } else { ?>
					<img src="<?php the_sub_field('current_track_image'); ?>" alt="<?php the_sub_field('current_track_name'); ?>">
					<?php } ?>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<div class="content-main-left">
	<div class="instructor-img-wrap">
		<img src="<?php the_field('instructor_image'); ?>" alt="">
	</div>
</div>
