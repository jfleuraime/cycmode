<?php
/**
 * Template part for displaying carreers.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cycmode
 */

?>

<article id="careers-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
</article><!-- #post-## -->
