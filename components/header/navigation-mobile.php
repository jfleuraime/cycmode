<?php
function mobile_menu_wrap() {
  $menu = '<ul id="%1$s" class="%2$s">';
  $menu .= '%3$s';
  if ( !is_user_logged_in() ) {
    $menu .= '<li class="auth-link"><a href="';
    $menu .= home_url('/login');
    $menu .= '" class="menu-login">Log In</a></li><li><a href="';
    $menu .= home_url('/register');
    $menu .= '" class="menu-join">Join</a></li>';
  } else {
    $menu .= '<li class="auth-link"><a href="';
    $menu .= home_url('/logout');
    $menu .= '" class="menu-logout">Logout</a></li>';
  }
  $menu .= '</ul>';
  return $menu;
}
?>
<nav id="mobile-navigation" role="navigation">
  <div id="mobile-nav-inner">
    <?php wp_nav_menu( array( 'theme_location' => 'top', 'menu_id' => 'top-menu', 'items_wrap' => mobile_menu_wrap()) ); ?>
    <div class="header-cta">
      <a href="<?php echo home_url('/buy-classes'); ?>" class="menu-cta button button-glow">Buy <span>Now</span></a>
    </div>
  </div>
</nav>
<!-- #mobile-navigation -->
