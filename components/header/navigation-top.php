<?php
  function desktop_menu_wrap() {
	$menu = '<ul id="%1$s" class="%2$s">';
	$menu .= '%3$s';
	if ( !is_user_logged_in()) {
	  if (is_page_template('page-auth.php')) {
		$menu .= '<li class="auth-link"><a href="';
		$menu .= home_url('/login');
		$menu .= '" class="menu-login">Log In</a><a href="';
		$menu .= home_url('/register');
		$menu .= '" class="menu-join">Join</a></li>';
	  } else {
		$menu .= '<li class="auth-link"><a href="#';
		$menu .= '" class="menu-login open-login">Log In</a><a href="#';
		$menu .= '" class="menu-join open-register">Join</a></li>';
	  }
	} else {
	  $current_user_id = get_current_user_id();
	  $profile_url = bp_loggedin_user_domain();
	  ob_start();?>
		<li class="auth-link">
		  Hi <a href="<?php echo $profile_url; ?>profile/" class="menu-profile"><?php bp_profile_field_data( 'field=First Name&user_id=' . $current_user_id ); ?></a>
		  <div class="profile-img">
			<div class="img-wrap">
			  <?php echo get_avatar($current_user_id); ?>
			</div>
		  </div>
		  <ul class="sub-menu">
			<?php if (current_user_can("manage_options")) { ?>
			<li><a href="<?php echo site_url(); ?>/wp-admin">View Dashboard</a></li>
			<?php } ?>
			<li><a href="<?php echo $profile_url; ?>profile/">View Profile</a></li>
			<li><a href="<?php echo $profile_url; ?>classes/">My Classes</a></li>
			<li><a href="<?php echo $profile_url; ?>settings/">Change Password</a></li>
			<li><a href="<?php echo wp_logout_url(); ?>" class="menu-logout">Logout</a></li>
		  </ul>
		</li>
	  <?php $menu .= ob_get_clean();
	}
	$menu .= '</ul>';
	return $menu;
  }
?>
 <nav id="site-navigation" class="main-navigation" role="navigation">
  <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
	<div class="menu-toggle-icon">
	  <span></span>
	  <span></span>
	  <span></span>
	  <span></span>
	</div>
	<div class="menu-toggle-text"><?php esc_html_e( 'Menu', 'cycmode' ); ?></div>
  </button>
  <div id="nav-inner">
	<?php wp_nav_menu( array( 'theme_location' => 'top', 'menu_id' => 'top-menu', 'items_wrap' => desktop_menu_wrap()) ); ?>
	<div class="header-cta">
	  <a href="<?php echo home_url('/buy-classes'); ?>" class="menu-cta button button-glow">Buy <span>Now</span></a>
	</div>
  </div>
</nav>
<!-- #site-navigation -->
