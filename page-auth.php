<?php
/**
 * Template Name: Auth Page
 */


get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-body">
			<div class="content-auth-box">
				<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				<?php get_template_part( 'components/page/content', 'page' ); ?>
			</div>
		</div>

		<?php endwhile; ?>

	</main>
</div>
<?php
get_footer();
