<?php
/**
 * Template Name: Test Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>

		<div class="content-body">
			<?php ;?>
		</div>

		<?php endwhile; ?>

	</main>
</div>
<?php
get_footer();
