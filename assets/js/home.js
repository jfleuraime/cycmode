(function ($) {
	var headerHeight = $('#masthead').outerHeight();
	var footerHeight = $('#colophon').outerHeight();
	var slide = $('.slide');

	$(window).load(function () {
		$('#home-slider').slick({
			autoplay: true,
			autoplaySpeed: 2000,
			arrows: false,
			infinite: true
		});
	});

	$('#home-slider').on('init', function () {

		setTimeout(function () {
			$('#home-slider').addClass('loaded');
		}, 1000);

	});

	$(window).smartresize(function () {

		var viewportWidth = window.innerWidth;
		var viewportHeight = window.innerHeight;

		slide.width(viewportWidth).height(viewportHeight - headerHeight - footerHeight);

		var currentSlide = $('.slider').slick('slickCurrentSlide');

		$('#home-slider').slick('slickGoTo', currentSlide);

	});

})(jQuery);
