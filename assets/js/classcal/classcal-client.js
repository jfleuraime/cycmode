(function ($) {
	$(document).ready(function () {

		window.location.hash = '';

		var classEvent = {};
		var modal = $('#modal-reserve-class');
		var modalClassInstructor = modal.find('.class-name-instructor');
		var modalInstructorPic = modal.find('.class-instructor-pic');
		var modalDate = modal.find('h3');

		var transitionEnd = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
		var transitionsSupported = ($('.csstransitions').length > 0);
		//if browser does not support transitions - use a different event to trigger them
		if (!transitionsSupported) transitionEnd = 'noTransition';

		//should add a loding while the events are organized

		function SchedulePlan(element) {
			this.element = element;
			this.timeline = this.element.find('.timeline');
			this.timelineItems = this.timeline.find('li');
			this.timelineItemsNumber = this.timelineItems.length;
			this.timelineStart = getScheduleTimestamp(this.timelineItems.eq(0).text());
			//need to store delta (in our case half hour) timestamp
			this.timelineUnitDuration = getScheduleTimestamp(this.timelineItems.eq(1).text()) - getScheduleTimestamp(this.timelineItems.eq(0).text());

			this.eventsWrapper = this.element.find('.events');
			this.eventsGroup = this.eventsWrapper.find('.events-group');
			this.singleEvents = this.eventsGroup.find('.single-event');
			this.eventSlotHeight = this.eventsGroup.eq(0).children('.top-info').outerHeight();

			this.animating = false;

			this.initSchedule();
		}

		SchedulePlan.prototype.initSchedule = function () {
			this.scheduleReset();
			this.initEvents();
		};

		SchedulePlan.prototype.scheduleReset = function () {
			var mq = this.mq();
			if (mq == 'desktop' && !this.element.hasClass('js-full')) {
				//in this case you are on a desktop version (first load or resize from mobile)
				this.eventSlotHeight = this.eventsGroup.eq(0).children('.top-info').outerHeight();
				this.element.addClass('js-full');
				this.placeEvents();
				this.element.hasClass('modal-is-open');
			} else if (mq == 'mobile' && this.element.hasClass('js-full')) {
				//in this case you are on a mobile version (first load or resize from desktop)
				this.element.removeClass('js-full loading');
				this.eventsGroup.children('ul').add(this.singleEvents).removeAttr('style');
				this.eventsWrapper.children('.grid-line').remove();
				//this.element.hasClass('modal-is-open') && this.checkEventModal();
				this.element.hasClass('modal-is-open');
			} else if (mq == 'desktop' && this.element.hasClass('modal-is-open')) {
				//on a mobile version with modal open - need to resize/move modal window
				this.element.removeClass('loading');
			} else {
				this.element.removeClass('loading');
			}
		};

		SchedulePlan.prototype.initEvents = function () {
			var self = this;

			this.singleEvents.each(function () {
				var thisClass = $(this);
				//create the .event-date element for each event
				/*var durationLabel = '<span class="event-date">' + $(this).data('start') + ' - ' + $(this).data('end') + '</span>';
				$(this).children('a').prepend($(durationLabel));*/

				//detect click on the event and open the modal
				$(this).on('click', 'a', function (event) {
					event.preventDefault();
					classEvent = {
						classID: thisClass.data('class-id'),
						classType: thisClass.data('class-type'),
						classDate: thisClass.data('class-date'),
						classSeats: thisClass.data('class-seats'),
						instructorName: thisClass.data('instructor-name'),
						instructorPic: thisClass.data('instructor-pic'),
					};
					setupModal(classEvent);
				});
			});
		};

		SchedulePlan.prototype.placeEvents = function () {
			var self = this;
			this.singleEvents.each(function () {
				//place each event in the grid -> need to set top position and height
				var start = getScheduleTimestamp($(this).attr('data-start')),
					duration = getScheduleTimestamp($(this).attr('data-end')) - start;

				var eventTop = self.eventSlotHeight * (start - self.timelineStart) / self.timelineUnitDuration,
					eventHeight = self.eventSlotHeight * duration / self.timelineUnitDuration;

				$(this).css({
					top: (eventTop - 1) + 'px',
					height: (eventHeight + 1) + 'px'
				});
			});

			this.element.removeClass('loading');
		};

		SchedulePlan.prototype.mq = function () {
			//get MQ value ('desktop' or 'mobile')
			var self = this;
			return window.getComputedStyle(this.element.get(0), '::before').getPropertyValue('content').replace(/["']/g, '');
		};


		var schedules = $('.cd-schedule');
		var objSchedulesPlan = [],
			windowResize = false;

		if (schedules.length > 0) {
			schedules.each(function () {
				//create SchedulePlan objects
				objSchedulesPlan.push(new SchedulePlan($(this)));
			});
		}

		$(window).on('resize', function () {
			if (!windowResize) {
				windowResize = true;
				(!window.requestAnimationFrame) ? setTimeout(checkResize): window.requestAnimationFrame(checkResize);
			}
		});

		function checkResize() {
			objSchedulesPlan.forEach(function (element) {
				element.scheduleReset();
			});
			windowResize = false;
		}

		function getScheduleTimestamp(time) {
			//accepts hh:mm format - convert hh:mm to timestamp
			time = time.replace(/ /g, '');
			var timeArray = time.split(':');
			var timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
			return timeStamp;
		}

		function loadAvailableSeats() {
			/*
			 * data.append('action', 'wlw_ajax_edit_profile');
			 * jQuery.ajax({
				url: classcal.ajaxurl, //from wp_localize_script
				type: 'POST',
				data: data,
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function (response) {
					if (response && response.success) {
						self.tabs.goto(2, true);
						if (response.type == 'edit_profile') {
							var locationA = $('.popup-ep form.edit-profile').find('input[name="city"]').val();
							var locationB = $('.popup-ep form.edit-profile').find('select[name="state"]').val();

							if (locationB && locationB !== 'other') {
								locationA = locationA + ', ' + locationB;
							}
							$('#profile-location').text(locationA);
							$('.profile-menu .menu-name').text($('.popup-ep form.edit-profile input[name="first_name"]').val());
						} else if (response.type == 'edit_bio') {
							$('#profile-bio').text($('.popup-ep form.edit-bio').find('textarea').val()).addClass('edited');
						}
					} else {
						alert(response.error);
					}
					self.$submit.attr('disabled', false).text("Save Changes");

				},
				fail: function () {
					alert('There was an error updating your profile');
					self.$submit.attr('disabled', true).text("Please Wait...");
				}
			});*/
		}

		// Add class event to modal
		function setupModal(classEvent) {
			console.log(classEvent);
			modalClassInstructor.html(classEvent.classType + ' with <span>' + classEvent.instructorName + '</span>');
			//modalInstructorPic.html('<img src="' + classEvent.instructorPic + '">');
			modalDate.html(classEvent.classDate);
			window.location.hash = 'modal-reserve-class';
		}

		// Reset modal
		function resetModal() {
			modalClassInstructor.html('');
			modalInstructorPic.html('');
			modalDate.html('');
			classEvent = {};
		}

		function reserveClass() {

		}

		$(document).on('cssmodal:show', function (event) {
			//window.location.hash = '';
		});

		// Reset modal when pressing close
		$(document).on('cssmodal:hide', function (event) {
			resetModal();
		});

	});

})(jQuery);
