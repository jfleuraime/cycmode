(function ($) {
	$(document).ready(function () {
		// Studio Seating Chart
		var avalailabeSpots = new Array(),
			availableSpotsInput = $('#studio_seats_comma_list'),
			seatingGrid = $('#seating-grid'),
			seatingGridCells = seatingGrid.find('.classcal-seating-grid-cell'),
			seatingGridSpots = seatingGrid.find('.classcal-seating-grid-spot');

		function generateSeatList() {
			seatingGridCells.each(function () {
				var status = '';
				if ($(this).hasClass('active')) {
					status = 'active';
				} else {
					status = 'in-active';
				}
				avalailabeSpots.push({
					'status': status
				});
			})
			availableSpotsInput.val(encodeURIComponent(JSON.stringify(avalailabeSpots)));
			avalailabeSpots = [];
		}

		seatingGridCells.on('click', function () {
			var seatRow = $(this).data('spot-row');
			var seatColumn = $(this).data('spot-column');
			var seatSpot = $(this).children('.classcal-seating-grid-spot');

			if ($(this).hasClass('active')) { // active
				$(this).removeClass('active');
				$(this).addClass('in-active');
			} else { // in active
				$(this).removeClass('in-active');
				$(this).addClass('active');
			}
			generateSeatList();
		})

	});
})(jQuery);
