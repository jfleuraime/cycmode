(function ($) {
  var settings = {
	rows: 8,
	cols: 8,
	rowCssPrefix: 'row-',
	colCssPrefix: 'col-',
	seatWidth: 35,
	seatHeight: 35,
	seatCss: 'seat',
	selectedSeatCss: 'selectedSeat',
	selectingSeatCss: 'selectedSeat'
  };

  var init = function (reservedSeat) {
	var str = [],
	  seatNo, className;
	for (i = 0; i < settings.rows; i++) {
	  for (j = 0; j < settings.cols; j++) {
		seatNo = (j + (i * settings.rows) + 1);
		className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
		if ($.isArray(reservedSeat) && $.inArray(seatNo, reservedSeat) != -1) {
		  className += ' ' + settings.selectedSeatCss;
		}
		str.push('<li class="' + className + '"' +
		  'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
		  '<a title="' + seatNo + '">' + seatNo + '</a>' +
		  '</li>');
	  }
	}
	$('#seating-chart-spots').html(str.join(''));
  };
  //case I: Show from starting
  //init();

  //Case II: If already booked

  $(document).ready(function () {

	var bookedSeats = $('#chart_spots').val().split(',');
	for (a in bookedSeats ) {
	  bookedSeats[a] = parseInt(bookedSeats[a], 10);
	}
	init(bookedSeats);


	$('.' + settings.seatCss).click(function () {
	  //if ($(this).hasClass(settings.selectedSeatCss)) {
		//alert('This seat is already reserved');
	  //} else {
		$(this).toggleClass(settings.selectingSeatCss);
	  //}
	});

	$('#btnShow').click(function () {
	  var str = [];
	  $.each($('#seating-chart-spots li.' + settings.selectedSeatCss + ' a, #seating-chart-spots li.' + settings.selectingSeatCss + ' a'), function (index, value) {
		str.push($(this).attr('title'));
	  });
	  $('#chart_spots').val(str.join(','));

	  alert('Seating chart updated. Click "Publish/Update" to save changes.');
	})
  });


})(jQuery);
