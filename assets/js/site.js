(function ($) {

  $(window).resize();

  // Slideout Menu
  var slideout = new Slideout({
    'panel': document.getElementById('page'),
    'menu': document.getElementById('mobile-navigation'),
    'padding': 256,
    'tolerance': 70
  });

  document.querySelector('.menu-toggle').addEventListener('click', function () {
    $(this).find('.menu-toggle-icon').toggleClass('open');
    slideout.toggle();
  });

  slideout.on('translateend', function () {
    setTimeout(function () {
      if (!slideout.isOpen()) {
        $('.menu-toggle-icon').removeClass('open');
      } else {
        $('.menu-toggle-icon').addClass('open');
      }
    }, 250);
  });

  $('#mini-carousel').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  });

  $(".open-register, .open-login").click(function () {
    if (!$("body").hasClass("slidedown")) { // If slidedown closed
      $("body").addClass("slidedown")
    }
  });

  $(".close-slidedown").click(function () {
    if ($("body").hasClass("slidedown")) { // If slidedown open
      $("body").removeClass("slidedown")
    }
  });

  $(".open-login").click(function () {
    $(".register-tab").removeClass("open");
    $(".login-tab").addClass("open");
  });

  $(".open-register").click(function () {
    $(".login-tab").removeClass("open");
    $(".register-tab").addClass("open");
  });

  $(".open-info").click(function () {
    $(".signup-section").removeClass("open");
    $(".info-section").addClass("open");
  });

  $(".open-signup").click(function () {
    $(".info-section").removeClass("open");
    $(".signup-section").addClass("open");
  });


})(jQuery);
