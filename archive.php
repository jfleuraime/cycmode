<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package cycmode
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

      <?php
      $show_submenu = get_field('show_submenu', 'option');
      $select_submenu = get_field('select_submenu', 'option'); ?>

      <div class="content-header <?php echo $show_submenu; ?>">
        <h1 class="page-title"><?php echo str_replace('Archives: ','',get_the_archive_title()); ?></h1>
      </div>
      <div class="content-body">
        <?php if ($show_submenu == 'submenu-visible' && $select_submenu) {
  get_template_part( 'components/header/navigation', 'submenu' );
} ?>
      <?php
    if ( have_posts() ) : ?>
        <?php
      /* Start the Loop */
      while ( have_posts() ) : the_post();

        /*
         * Include the Post-Format-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
         */
        get_template_part( 'components/post/content', get_post_format() );

      endwhile;

      the_posts_navigation();

    else :

      get_template_part( 'components/post/content', 'none' );

    endif; ?>
      </div>
    </main>
  </div>
  <?php
get_footer();
