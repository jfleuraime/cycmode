<?php
/**
 * Template Name: Class Schedule
 */

get_header();

// Validate url param dates
function validateDate($date, $format = 'Y-m-d H:i:s'){
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

// Hours per day (24hrs)
$day_start = strtotime('08:00');
$day_end = strtotime('23:00');

// Get days of this week
$schedule_week_number = date("W");
$schedule_year = date("Y");
$schedule_week_days = array();

// Get url params
$filter_host = $_GET['host'];
$filter_date = $_GET['date'];
$filter_category = $_GET['cat'];
$filter_location = $_GET['studio'];

if (!empty($filter_date) && validateDate($filter_date, 'Y-m-d')) {
	$schedule_week_number = date("W", strtotime($filter_date));
	$schedule_year = date("Y", strtotime($filter_date));
}

// Create week day array
for($day=1; $day<=7; $day++){
	$week_day = array(
		'name' => date('l', strtotime($schedule_year."W".$schedule_week_number.$day)),
		'short_month' => date('M', strtotime($schedule_year."W".$schedule_week_number.$day)),
		'day' => date('j', strtotime($schedule_year."W".$schedule_week_number.$day)),
		'full_date' => date('Ymd', strtotime($schedule_year."W".$schedule_week_number.$day)),
	);
	$schedule_week_days[] = $week_day;
}
// Monday of last/next week
$schedule_last_week = date('Y-m-d', strtotime($schedule_week_days[0]['full_date'] .' -1 week'));
$schedule_next_week = date('Y-m-d', strtotime($schedule_week_days[0]['full_date'] .' +1 week'));
$today_date = date('Ymd'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();?>

				<div class="content-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<a href="<?php echo add_query_arg('date',$schedule_last_week, get_permalink());?>">Last</a>
					<div class="">Week of <span><?php echo date('M j, Y', strtotime($schedule_week_days[0]['full_date']));?></span></div>
					<a href="<?php echo add_query_arg('date',$schedule_next_week, get_permalink());?>">Next</a>
				</div>
				<?php endwhile; ?>

				<!-- Schedule -->
					<div class="cd-schedule loading">
						<div class="timeline">
							<ul>
								<?php for ($i=$day_start; $i<=$day_end; $i = $i + 30*60) {
									echo '<li><span>'.date('g:i A',$i).'</span></li>';
								}?>
							</ul>
						</div>
						<!-- .timeline -->

						<div class="events">
							<ul>
							<?php
								// Get this weeks classes
								foreach($schedule_week_days as $schedule_day) {
								$args = array (
									'post_type' => 'classcal_classes',
									'meta_query' => array(
										array(
											'key' => 'class_date',
											'value' => $schedule_day['full_date'],
											'compare' => '='
										)
									),
								);
								$schedule_day_class = '';
								if ($schedule_day['full_date'] == $today_date) {
									$schedule_day_class = 'today';
								}
								$schedule_week_classes = get_posts($args);?>
								<li class="events-group <?php echo $schedule_day_class;?>">
									<div class="top-info">
									<span><?php echo $schedule_day['name']?></span>
									<span><?php echo $schedule_day['short_month']; ?></span>
									<span><?php echo $schedule_day['day']; ?></span>
									</div>
									<ul>
									<?php foreach($schedule_week_classes as $class) {
										$post = $class;
										setup_postdata($post);
										$instructor_post = get_field('instructor_id');
										$instructor_id = '';
										$instructor_name = '';
										$instructor_pic = '';
										if ($instructor_post){
											setup_postdata($post);
											$post = $instructor_post;
											$instructor_name = $post->post_title;
											$instructor_id = $post->ID;
											$instructor_pic = get_field('instructor_image');
											$post = $class;
											setup_postdata($post);
										}
										$start_time = get_field('class_start_time');
										$end_time = get_field('class_end_time');
										$class_type_id = get_field('class_type');
										$class_type = get_term_by('id', $class_type_id, 'classcal_cat');
										$class_type_name = $class_type->name; ?>
										<li class="single-event" data-start="<?php echo date('H:i', strtotime($start_time)); ?>" data-end="<?php echo date('H:i', strtotime($end_time)); ?>" data-class-id="<?php echo $post->ID; ?>" data-class-type="<?php echo $class_type_name;?>" data-class-date="<?php echo date('l, M j', strtotime($schedule_day['full_date'])); ?> @ <?php echo date('h:i a', strtotime($start_time)); ?>" data-class-seats="" data-instructor-id="<?php echo $instructor_id; ?>" data-instructor-name="<?php echo $instructor_name;?>" data-instructor-pic="<?php echo $instructor_pic;?>">
											<a href="#modal-reserve-class">
												<em class="event-name"><?php echo $class_type_name;?></em>
												<button type="button" class="button">Reserve</button>
											</a>
										</li>
										<?php wp_reset_postdata();
									}?>
									</ul>
								</li>
							<?php } ?>
							</ul>
						</div>
					</div>
					<!-- .cd-schedule -->

		</main>
	</div>
	<?php
get_footer();?>

<!-- Reserve Schedule Modal -->
<section class="modal--fade" id="modal-reserve-class" data-stackable="false" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-inner">
		<div class="modal-header">
			<div class="class-name-instructor"></div>
			<h3></h3>
		</div>
		<div class="modal-body">
			<div class="class-instructor-pic"></div>
				<?php classcal_seating_grid();?>
		</div>
		<div class="modal-bottom">
			<button type="button" id="reserve-seat">Reserve</button>
			<input type="hidden" name="reserve_class_nonce" id="reserve_class_nonce" value="'<?php wp_create_nonce('reserve_class_nonce');?>'" />
		</div>
	</div>

	<a href="#!" class="modal-close" title="Close this modal"
		 data-dismiss="modal" data-close="Close">&times;</a>
</section>
