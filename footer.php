<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cycmode
 */

?>

  </div>
  <footer id="colophon" class="site-footer" role="contentinfo">
    <div class="pure-g footer-wrap">
      <div class="pure-u-1-1 pure-u-xl-1-3">
        <?php get_template_part( 'components/footer/bottom', 'info' ); ?>
      </div>
      <div class="pure-u-1-1 pure-u-xl-2-3">
        <?php get_template_part( 'components/footer/bottom', 'menu' ); ?>
      </div>
    </div>
  </footer>
</div>
<?php
if (!is_user_logged_in()) {
  if (!is_page_template('page-auth.php')) {
    get_template_part( 'components/footer/bottom', 'slidedown' );
  }
}

wp_footer(); ?>

</body>
</html>
