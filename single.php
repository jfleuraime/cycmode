<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package cycmode
 */

get_header(); ?>

  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php
    while ( have_posts() ) : the_post(); ?>
      <div class="content-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
      </div>

      <div class="content-body">
        <?php $post_type = get_post_type();
        get_template_part( 'components/post/content', $post_type );

      // If comments are open or we have at least one comment, load up the comment template.
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;?>
      </div>
     <?php endwhile; // End of the loop.
    ?>

    </main>
  </div>
<?php
get_footer();


