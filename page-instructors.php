<?php
/**
 * Template Name: Instructors Page
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-body">
			<div class="content-main-right">
				<h1 class="entry-title">Our Instructors</h1>
				<?php if( get_field('instructors_featured_text') ) { ?><p class="intro"><?php the_field('instructors_featured_text'); ?></p><?php } ?>
			</div>

			<?php endwhile; ?>

			<?php
			$query = new WP_Query(array('post_type' => 'instructors'));

			if ( $query->have_posts() ) :
			while ( $query->have_posts() ) : $query->the_post();
			get_template_part( 'components/loop/loop', 'instructor' );
			endwhile;
			//the_posts_navigation();
			endif;
			wp_reset_postdata(); ?>
		</div>

	</main>
</div>
<?php
get_footer();
