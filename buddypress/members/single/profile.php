<?php
/**
 * BuddyPress - Users Profile
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<div class="item-list-tabs no-ajax" id="subnav" role="navigation">
  <ul>
    <?php bp_get_options_nav(); ?>
  </ul>
</div><!-- .item-list-tabs -->

<?php
echo '<div class="content-member-body-inner">';
/**
 * Fires before the display of member profile content.
 *
 * @since 1.1.0
 */
do_action( 'bp_before_profile_content' ); ?>

<div class="profile">

<?php switch ( bp_current_action() ) :

  // Edit
  case 'edit'   :
    bp_get_template_part( 'members/single/profile/edit' );
    break;

  // Change Avatar
  case 'change-avatar' :
    bp_get_template_part( 'members/single/profile/change-avatar' );
    break;

  // Change Cover Image
  case 'change-cover-image' :
    bp_get_template_part( 'members/single/profile/change-cover-image' );
    break;

  // Compose
  case 'public' :

    // Display Dashboard / Activity
    bp_get_template_part( 'members/single/profile/dashboard' );
    break;

  // Any other
  default :
    bp_get_template_part( 'members/single/plugins' );
    break;
endswitch; ?>
</div><!-- .profile -->

<?php
echo '</div>';
/**
 * Fires after the display of member profile content.
 *
 * @since 1.1.0
 */
do_action( 'bp_after_profile_content' ); ?>
