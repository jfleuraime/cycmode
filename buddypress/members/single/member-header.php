<?php
/**
 * BuddyPress - Users Header
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php

/**
 * Fires before the display of a member's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_member_header' ); ?>

<div id="item-header-avatar">
  <a href="<?php bp_displayed_user_link(); ?>">

    <?php bp_displayed_user_avatar( 'type=full' ); ?>

  </a>
</div><!-- #item-header-avatar -->

<div id="item-header-content">
  <h2><?php echo bp_get_displayed_user_fullname(); ?></h2>

  <?php

  /**
   * Fires before the display of the member's header meta.
   *
   * @since 1.2.0
   */
  do_action( 'bp_before_member_header_meta' ); ?>

  <div id="item-meta">

    <?php if ( bp_is_active( 'activity' ) ) : ?>

      <div id="latest-update">

        <?php //bp_activity_latest_update( bp_displayed_user_id() ); ?>

      </div>

    <?php endif; ?>

    <div id="item-buttons">

      <?php

      /**
       * Fires in the member header actions section.
       *
       * @since 1.2.6
       */
      do_action( 'bp_member_header_actions' ); ?>

    </div><!-- #item-buttons -->

    <?php

     /**
      * Fires after the group header actions section.
      *
      * If you'd like to show specific profile fields here use:
      * bp_member_profile_data( 'field=About Me' ); -- Pass the name of the field
      *
      * @since 1.2.0
      */
     do_action( 'bp_profile_header_meta' );

     ?>

  </div><!-- #item-meta -->

</div><!-- #item-header-content -->

<?php

/**
 * Fires after the display of a member's header.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_member_header' ); ?>

<?php

/** This action is documented in bp-templates/bp-legacy/buddypress/activity/index.php */
do_action( 'template_notices' ); ?>
