<?php
/**
 * BuddyPress - Members Single Profile
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */
do_action( 'bp_before_member_settings_template' ); ?>

<form action="<?php echo bp_displayed_user_domain() . bp_get_settings_slug() . '/general'; ?>" method="post" class="standard-form" id="settings-form">

  <?php if ( !is_super_admin() ) : ?>

	<input type="password" name="pwd" id="pwd" size="16" value="" placeholder="Current Password" class="settings-input small" <?php bp_form_field_attributes( 'password' ); ?>/> &nbsp;<a href="<?php echo wp_lostpassword_url(); ?>" title="<?php esc_attr_e( 'Password Lost and Found', 'buddypress' ); ?>"><?php _e( 'Lost your password?', 'buddypress' ); ?></a>

  <?php endif; ?>

  <input type="password" name="pass1" id="pass1" size="16" placeholder="New Password" value="" class="settings-input small password-entry" <?php bp_form_field_attributes( 'password' ); ?>/>

	<input type="password" name="pass2" id="pass2" size="16" value="" placeholder="Repeat New Password" class="settings-input small password-entry-confirm" <?php bp_form_field_attributes( 'password' ); ?>/>

  <?php

  /**
   * Fires before the display of the submit button for user general settings saving.
   *
   * @since 1.5.0
   */
  do_action( 'bp_core_general_settings_before_submit' ); ?>

  <div class="submit">
	<input type="submit" name="submit" value="<?php esc_attr_e( 'Save Changes', 'buddypress' ); ?>" id="submit" class="auto" />
  </div>

  <?php

  /**
   * Fires after the display of the submit button for user general settings saving.
   *
   * @since 1.5.0
   */
  do_action( 'bp_core_general_settings_after_submit' ); ?>

  <?php wp_nonce_field( 'bp_settings_general' ); ?>

</form>

<?php

/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */
do_action( 'bp_after_member_settings_template' ); ?>
