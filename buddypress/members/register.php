<?php
/**
 * BuddyPress - Members Register
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<div id="buddypress">

	<?php

	/**
	 * Fires at the top of the BuddyPress member registration page template.
	 *
	 * @since 1.1.0
	 */
	do_action( 'bp_before_register_page' ); ?>

	<div class="page" id="register-page">

		<form action="<?php if (!is_page_template('page-auth.php')) { echo bp_get_signup_page(); } ?>" name="signup_form" id="signup_form" class="standard-form" method="post" enctype="multipart/form-data">

		<?php if ( 'registration-disabled' == bp_get_current_signup_step() ) : ?>
			<?php

			do_action( 'template_notices' ); ?>
			<?php

			do_action( 'bp_before_registration_disabled' ); ?>

				<p><?php _e( 'User registration is currently not allowed.', 'buddypress' ); ?></p>

			<?php

			do_action( 'bp_after_registration_disabled' ); ?>
		<?php endif; // registration-disabled signup step ?>

		<?php if ( 'request-details' == bp_get_current_signup_step() ) : ?>

			<?php

			do_action( 'template_notices' ); ?>


			<?php

			do_action( 'bp_before_account_details_fields' ); ?>

			<div class="signup-section open">

				<?php if (!is_page_template('page-auth.php')) {?>
				<h3><span class="active">Create Account</span><span>Your Info</span></h3>
				<?php }?>
				<div class="form-group">
				<label for="signup_username"><?php _e( 'Username', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
				<?php

				do_action( 'bp_signup_username_errors' ); ?>
				<input type="text" name="signup_username" id="signup_username" value="<?php bp_signup_username_value(); ?>" placeholder="Username" <?php bp_form_field_attributes( 'username' ); ?>/>
				</div>

				<div class="form-group">
				<label for="signup_email"><?php _e( 'Email Address', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
				<?php

				do_action( 'bp_signup_email_errors' ); ?>
				<input type="email" name="signup_email" id="signup_email" value="<?php bp_signup_email_value(); ?>" placeholder="Email Address" <?php bp_form_field_attributes( 'email' ); ?>/>
				</div>

				<div class="form-group-wide">
				<label for="signup_password"><?php _e( 'Choose a Password', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
				<?php

				do_action( 'bp_signup_password_errors' ); ?>
				<input type="password" name="signup_password" id="signup_password" value="" placeholder="Password" class="password-entry" <?php bp_form_field_attributes( 'password' ); ?>/>

				<label for="signup_password_confirm"><?php _e( 'Confirm Password', 'buddypress' ); ?> <?php _e( '(required)', 'buddypress' ); ?></label>
				<?php

				do_action( 'bp_signup_password_confirm_errors' ); ?>
				<input type="password" name="signup_password_confirm" id="signup_password_confirm" placeholder="Confirm Password" value="" class="password-entry-confirm" <?php bp_form_field_attributes( 'password' ); ?>/>
				</div>


				<?php

				do_action( 'bp_account_details_fields' );

				if (!is_page_template('page-auth.php')) {?>
				<div class="submit">
					<span>Already a member? </span><a href="#" class="open-login">Log In</a>
					<button type="button" class="button open-info">Next</button>
				</div>
				<?php }?>

			</div><!-- #basic-details-section -->

			<?php

			do_action( 'bp_after_account_details_fields' ); ?>

			<?php /***** Extra Profile Details ******/ ?>

			<?php if ( bp_is_active( 'xprofile' ) ) : ?>

				<?php

				do_action( 'bp_before_signup_profile_fields' ); ?>

				<div class="info-section">

					<?php if (!is_page_template('page-auth.php')) {?>
					<h3><span>Create Account</span><span class="active">Your Info</span></h3>
					<?php }?>

					<?php /* Use the profile field loop to render input fields for the 'base' profile field group */ ?>
					<?php if ( bp_is_active( 'xprofile' ) ) : if ( bp_has_profile( array( 'profile_group_id' => 1, 'fetch_field_data' => false ) ) ) : while ( bp_profile_groups() ) : bp_the_profile_group(); ?>

					<?php while ( bp_profile_fields() ) : bp_the_profile_field(); ?>

						<div<?php bp_field_css_class( 'form-group' ); ?>>

							<?php
							$field_type = bp_xprofile_create_field_type( bp_get_the_profile_field_type() );
							$field_type->edit_field_html();

							do_action( 'bp_custom_profile_edit_fields_pre_visibility' ); ?>

							<?php

							do_action( 'bp_custom_profile_edit_fields' ); ?>

						</div>

					<?php endwhile; ?>

					<input type="hidden" name="signup_profile_field_ids" id="signup_profile_field_ids" value="<?php bp_the_profile_field_ids(); ?>" />

					<?php endwhile; endif; endif; ?>

					<?php

					do_action( 'bp_signup_profile_fields' );?>

				<!-- #profile-details-section -->

				<?php

				do_action( 'bp_after_signup_profile_fields' ); ?>

			<?php endif; ?>


			<?php

			do_action( 'bp_before_registration_submit_buttons' ); ?>

			<div class="submit">
				<?php if (!is_page_template('page-auth.php')) {?>
					<button type="button" class="button button-wht open-signup">Back</button>
				<?php } else {?>
					<span>Already a member? </span><a href="<?php echo home_url('/login'); ?>">Log In</a>
				<?php } ?>
				<input type="submit" name="signup_submit" id="signup_submit" value="<?php esc_attr_e( 'Sign Up', 'buddypress' ); ?>" />
			</div>
			</div>

			<?php

			do_action( 'bp_after_registration_submit_buttons' ); ?>

			<?php wp_nonce_field( 'bp_new_signup' ); ?>

		<?php endif; // request-details signup step ?>

		<?php if ( 'completed-confirmation' == bp_get_current_signup_step() ) : ?>

			<?php

			do_action( 'template_notices' ); ?>
			<?php

			do_action( 'bp_before_registration_confirmed' ); ?>

			<?php if ( bp_registration_needs_activation() ) : ?>
				<p><?php _e( 'You have successfully created your account! To begin using this site you will need to activate your account via the email we have just sent to your address.', 'buddypress' ); ?></p>
			<?php else : ?>
				<p><?php _e( 'You have successfully created your account! Please log in using the username and password you have just created.', 'buddypress' ); ?></p>
			<?php endif; ?>

			<?php

			do_action( 'bp_after_registration_confirmed' ); ?>

		<?php endif; // completed-confirmation signup step ?>

		<?php


		do_action( 'bp_custom_signup_steps' ); ?>

		</form>

	</div>

	<?php

	do_action( 'bp_after_register_page' ); ?>

</div><!-- #buddypress -->
