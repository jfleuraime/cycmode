<?php
/**
 * Template Name: Auth Page
 */


get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<div class="content-body">
			<div class="content-auth-box content-auth-box-wide">
				<h2 class="entry-title">Activate Your Account</h2>
				<?php get_template_part( 'buddypress/members/activate'); ?>
			</div>
		</div>

		<?php endwhile; ?>

	</main>
</div>
<?php
get_footer();
