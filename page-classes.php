<?php
/**
 * Template Name: Classes Page - Old
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post();
		$page_layout = get_field('page_layout');
		$show_header_bg = get_field('show_header_bg');
		$show_submenu = get_field('show_submenu');
		$select_submenu = get_field('select_submenu'); ?>

		<div class="content-header <?php echo $show_header_bg; ?>">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div>

		<div class="content-body <?php echo $page_layout; ?>">
			<?php if ($show_submenu == 'submenu-visible' && $select_submenu) {
	get_template_part( 'components/header/navigation', 'submenu' );
} ?>
			<?php get_template_part( 'components/page/content', 'page' ); ?>
		</div>

		<?php endwhile; ?>

		</div>

	</main>
</div>
<?php
get_footer();
